import socket
import random
import sys
import time
import datetime
import threading
import setproctitle

class Neuron():

    # ====================================================

    def __init__(self, *argv):

        self.argv = argv
        self.port_number = argv[1] # Integer: 10000 - 65000
        self.spike_sign = argv[2] # 1 or -1 
        self.to_port_list = argv[3] # List of ports to which synapses connect
        self.synaptic_placticity = argv[4] # Boolean (0 = no Hebbian learning, 1 = Hebbian learning enabled)

        self.port = None
        self.start_time = time.time()
        self.is_alive = 1
        self.internal_potential = 0     # Internal soma potential
        self.output_threshold = 100
        self.output_signal = 80
        self.output_neuron_address_array = list(self.to_port_list.split(","))
        self.synaptic_connections = {}
        self.signal_sign = 1
        self.base_synaptic_factor = 50 # List of synaptic connections from other neurons (initially blank dictionary) (max: 100)
        self.base_synaptic_variance = 40 # For all new connections (must be: 0 < base_synaptic_variance < base_synaptic_factor)

    # ====================================================

    def pprint(self,value=""):
        #print ("["+(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))+"] - Port "+str(self.port)+": "+str(value))
        #sys.stdout.flush()
        pass

    def get_sign(self,value):
        return int(value/abs(value))

    def create(self,port):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind(("localhost", self.port))
        self.s.listen(100)

    def listen(self):
        conn, addr = self.s.accept()
        data = conn.recv(1024)
        try:
            # Unpack data
            self.pprint("Data Receive via a synapse: %s"%data)
            from_port,value = data.split('/')
            value = int(value)

            # Update synpatic weights (Hebbian learning)
            if (int(self.synaptic_placticity) == 0):

                # Fixed connections, initial weighting 100
                self.synaptic_connections[from_port] = [10, round(time.time()-self.start_time,4)]
                synaptic_strength = self.synaptic_connections[from_port][0] / 50.0
                value = int(value * synaptic_strength)
            
            else:
                # Hebbian learning synapses, random initial weighting
                if from_port not in self.synaptic_connections:
                    self.synaptic_connections[from_port] = [random.randrange(self.base_synaptic_factor-self.base_synaptic_variance,self.base_synaptic_factor+self.base_synaptic_variance), round(time.time()-self.start_time,4)]
                    self.pprint("Synaptic Connections (created): %s"%self.synaptic_connections)
                else:
                    self.synaptic_connections[from_port][1] = round(time.time()-self.start_time,4)
                    self.pprint("Synaptic Connections (updated time): %s"%self.synaptic_connections)

                # Scale value (taking into account the strength of synaptic weights)
                synaptic_strength = self.synaptic_connections[from_port][0] / 50.0
                value = int(value * synaptic_strength)

            # Update neuron threshold
            self.internal_potential += int(value)
            self.internal_potential = max(0,min(100,self.internal_potential)) # Max = 100, Minimum = 0
        except:
            pass

        # Manual commands (DEBUG and TERMINATION)
        try:
            if (data[0] == 'q'):
                self.is_alive = 0
                self.pprint("Neuron killed")
                return 0
            elif (data[0] == 's'):
                self.send(addr[1],[self.port, self.internal_potential])
                return 1
        except:
            pass
        conn.close()
        return 1

    def send(self,to_port, message):
        try:
            s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s2.connect(("localhost", to_port))
            s2.sendall(str(message))
            s2.close()
        except:
            return 0
        return 1

    # Decay threshold (exponential decay)
    def signal_monitor(self):

        # Log Internal threshold
        self.pprint("Threshold: %s"%self.internal_potential)
        setproctitle.setproctitle("python neuron [%s (%03d%%)] <%s> {%s}"%(int(self.port_number),self.internal_potential, self.synaptic_placticity, self.synaptic_connections))

        previous_internal_potential = self.internal_potential

        # FIRING Routine
        if (self.internal_potential >= self.output_threshold):

            # Send signal if there is a value output neuron array (list of postsynaptic synapses)
            if (self.output_neuron_address_array[0] != ""):

                # Send output signal
                for output_neuron_address in self.output_neuron_address_array:
                    message = "%s/%s"%(self.port,self.signal_sign*self.output_signal)  
                    try:
                        output_neuron_address = int(output_neuron_address)
                        self.send(output_neuron_address, message) # Apply inhibitory / excitatory condition here
                        self.pprint("Signal Sent to port %d"%output_neuron_address)
                    except:
                        self.pprint("FAILED to send signal, Message: %s"%(message))
                        pass
                # Reduce the input threshold
                self.internal_potential = 0

                # Update synpatic weights after firing
                for from_port in self.synaptic_connections.keys():
                    # (Min 0, Max 10, change +-)
                    sign = self.get_sign(0.1 - abs(self.synaptic_connections[from_port][1] - (time.time()-self.start_time)))
                    change = 0.1*self.synaptic_connections[from_port][0]*sign
                    new_weight = round(self.synaptic_connections[from_port][0]+change,3)
                    self.synaptic_connections[from_port][0] = min(100,max(0,new_weight))
                    self.pprint("Synaptic Connections (updated value): %s"%self.synaptic_connections)

            else:
                pass

        # Gradual decay of signal
        self.internal_potential = round((self.internal_potential - self.internal_potential/20.0),2)

        # Repeat
        if (self.is_alive == 1): 
            t = threading.Timer(0.1, self.signal_monitor)
            t.start()

    # ===================================================

    def run(self):

        # Get Port Number
        try:
            self.port = int(self.port_number)
        except:
            self.port = 10000 + int(10000 * random.random())

        # Neuron type (excitatory / inhibitory)
        try:
            self.signal_sign = int(self.spike_sign)
        except:
            self.signal_sign = 1 # Excitatory by default

        # Get Output Neuron Address
        try:
            self.output_neuron_address_array = list(self.to_port_list.split(","))
        except:
            self.output_neuron_address_array = list()

        self.pprint("output_neuron_address_array: %s"%self.output_neuron_address_array)

        # Create neuron
        for i in range (0,10):
            try:
                self.create(self.port)
                self.pprint ("Created neuron (address: %s, output: %s) - attempt #%s"%(self.port,self.output_neuron_address_array,i))
                break
            except:
                self.pprint ("FAILED to create neuron (address: %s, output: %s) - attempt #%s"%(self.port,self.output_neuron_address_array,i))
                if i == 9: 
                    exit()

        # Monitor neuron (thread)
        self.signal_monitor()

        loop_flag = 1
        while loop_flag == 1:
            loop_flag = self.listen()

# =================================================

if __name__ == '__main__':
    n = Neuron(*sys.argv)
    n.run()

# =================================================

