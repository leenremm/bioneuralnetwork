import os
import sys
import time
import random
from threading import Thread
import socket
import datetime
import threading
import drawnn
import matplotlib.pyplot as plt
from subprocess import Popen, PIPE

# ===============================================================

network_design = [9,9,9,1] # Generate neurons (layers)

base_port = 9999 # Placeholder port number for manual signals sent via command line, and for evalutation signal to be received

# ===============================================================


global is_alive; is_alive = 1

def pprint(value=""):
    print ("["+(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))+"] - Module: "+str(value))
    sys.stdout.flush()

def generate_unique_port_number(i,j):
    global port_list
    temp_port = 10000 + 1000*i + j;
    port_list.append(temp_port)
    return temp_port

def spawn_neuron(command):
    #os.system(command)
    p = Popen([sys.executable] + command, stdout=PIPE)
    return 1

def send(to_port, message):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(("localhost", to_port))
        s.sendall(str(message))
        s.close()
    except:
        return 0
    return 1

def isOpen(check_ip,check_port):
    check_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        check_sock.connect((check_ip, int(check_port)))
        check_sock.shutdown(2)
        return True
    except:
        return False

# ======================================================================

# List of all ports
port_list = []

# Generate nodes
network_ports = []
for i in range(0,len(network_design)):
    network_ports.append([])
    for j in range(0,network_design[i]):
        network_ports[i].append([])
        network_ports[i][j] = int(generate_unique_port_number(i, j))

# ======================================================================

# Check all ports
open_port_list = []
print "Checking all %d required ports... \n"%(len(port_list))
for i in port_list:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = isOpen("localhost", i)
    print "Checking if port",i,"is being used...",
    print result
    if result == True:
        open_port_list.append(str(i))
    sock.close()

if len(open_port_list) == 0:
    print "\nAll %s ports available. Safe to proceed with creating network."%(len(port_list))
else:
    print "\nNote: %s/%s (%s%%) ports already in use!"%(len(open_port_list), len(port_list), int(100*len(open_port_list)/len(port_list)))

key_press = raw_input("\nPress any key to show network diagram...")

# ======================================================================

# Links between neurons
network_links = []
for row_num in range(0,len(network_ports)-1):
    network_links.append([])
    # For each column (cell)
    for col_from_num in range(0,len(network_ports[row_num])):
        network_links[row_num].append([])
        # For each cell in next row
        links = []
        for col_to_num in range(0,len(network_ports[row_num+1])):
            links.append(network_ports[row_num + 1][col_to_num])
        network_links[row_num][col_from_num] = list(links)

# ======================================================

# NN Visualization Diagram
nn_dim = list()
for row in network_ports:
    nn_dim.append(len(row))
plt.figure()
plt.ion()
network = drawnn.DrawNN( nn_dim )
network.draw()
plt.show()

key_press = raw_input("\nPress any key to create network...")

# ================================================

# Create Neurons
print "\nCreating Spiking Neural Network:\n"

threads = []
timestamp = round(time.time())

for i in range(0,len(network_ports)):
    for j in range(0,len(network_ports[i])):
        filename = "%d_%d"%(timestamp, network_ports[i][j])
        port_number = network_ports[i][j]
        if (isOpen("localhost", port_number)):
            continue # Do not duplicate neurons
        spike_sign = 1 #Use: -1 for inhibitory neuron
        if (i == len(network_ports)-1):
            # Last row does not have connections            
            #command = "python neuron.py %s %s > log/%s.txt"%(port_number,spike_sign,filename)
            #command = "python neuron.py %s %s > /dev/null 2>&1"%(port_number,spike_sign)
            command = ["neuron.py", str(port_number), str(spike_sign), str(""), str(0)]
        else:
            # A row with connections
            to_port_list = ",".join(str(i) for i in network_links[i][j])
            #command = "python neuron.py %s %s %s > log/%s.txt"%(port_number,spike_sign,to_port_list,filename)
            #command = "python neuron.py %s %s %s > /dev/null 2>&1"%(port_number,spike_sign,to_port_list)
            command = ["neuron.py", str(port_number), str(spike_sign), str(to_port_list), str(1)]  
        threads.append(Thread(target=spawn_neuron, args=(command,)))
        threads[-1].daemon=False
        threads[-1].start()
        pprint ("Created Neuron (port: %s)"%(network_ports[i][j]))

while True:
    input_command = raw_input('\nEnter command (q = quit, or enter a neuron port number to send a value of 50): ')
    if (input_command == 'q'):
        print "\nKilling all neurons: \n"
        for p in port_list:
            send(p, 'q') # Kill each child thread (listener)
            pprint ("Killed Neuron (port: %s)"%(p))
        break
    else:
        try:
            send(int(input_command), "%s/%s"%(base_port,50))
            print "Sent"
        except:
            print "Error"
            pass

time.sleep(0.1)

# =========================================

print "\nDeleting Log Files... ",
for close_port in port_list:
    try:
        filename = "%d_%d"%(timestamp, close_port)
        os.remove("log/%s.txt"%(filename))
    except:
        pass
        #print "Error deleting file: "+filename
print "Done\n"

# =========================================






