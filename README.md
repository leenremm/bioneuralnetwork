=====================================================================

Author: Leen Remmelzwaal
Date: 24th May 218
License: CC BY-SA
Affiliation: University of Cape Town, South Africa

=====================================================================

**Setup:**

- Run the install/install.sh script to install dependencies

**Network**

- Create a network by running the network/module.py using python

**Visualization Tool**

-  Open a browser, and navigate to visualization/index.php. Note: This requires PHP to be installed.

**Random Images Streams**

- To start an stream of randomized images, run input/image_sequence.py using Python

=====================================================================
