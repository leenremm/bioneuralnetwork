<!DOCTYPE html>
<html>
  <head>
    <title>Spiking Neuron Network Process Monitor</title>
    <script type="text/javascript" src="smoothie.js"></script>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px;
        }
        table {
            border-collapse: collapse;
        }
        tr, td {
            border: 1px solid black;
            height: 25px;
        }
    </style>
  </head>
  <body>

    <h1>Spiking Neuron Network Process Monitor</h1>

    <?php 
    exec("php poll_ps.php", $file_contents);
    $master_data = json_decode("".join($file_contents), true);
    $ps_length = sizeof($master_data);
    if (sizeof($master_data) == 0) {print "No Neuron Processes found"; exit();}
    $keys = array_keys($master_data);

    // =======================================++
    // Key Grid (graphs)
    // =======================================++

    $key_grid = array();
    for ($j=0;$j<sizeof($key_grid[1]);$j++){
        for ($k=0;$k<5;$k++){
           $key_grid[1][$j][$k] = -1;
        }
    }
    foreach ($keys as $key)
    {
        $cc = floor($key/10000);
        $row = floor(($key-$cc*10000)/1000);
        $col = $key-$cc*10000-$row*1000;
        $key_grid[$cc][$col][$row] = 0;
    }
    $colours = array("#ffc6b3","#ffffb3","#c6ffb3","#b3ffd9","#b3ecff","#b3b3ff","#ecb3ff","#ffb3d9");

    // =======================================++
    // Image Grid
    // =======================================++

    $image_grid = array();
    for ($j=0;$j<sizeof($image_grid[1]);$j++){
        for ($k=0;$k<5;$k++){
           $image_grid[1][$k][$j] = -1;
        }
    }
    foreach ($keys as $key)
    {
        $cc = floor($key/10000);
        $row = floor(($key-$cc*10000)/1000);
        $col = $key-$cc*10000-$row*1000;
        $image_grid[$cc][$row][$col] = 0;
    }

    ?>

    <text>Neuron Processes Detected: <?php echo $ps_length;?> </text></br></br>

    <text>Value (graph) Update Counter: </text><text id="counter">0</text></br></br>

    <?php 
    foreach ($key_grid as $cc_key => $cc){ ?>

        <h2><u>Cortical Column: <?php echo $cc_key; ?></u></h2>

        <table border=1>
        <tr style="background:#CCC;">
            <th rowspan=2>Node</th>
            <th colspan=<?php echo sizeof($cc[0]);?>>Layer</th>
        </th>

        <tr style="background:#CCC;">
            <?php for ($i = 0; $i <= sizeof($cc[0])-1; $i++){ ?>
                <th style="background:<?php echo $colours[$i];?>;"><?php echo $i+1; ?></th>
            <?php } ?>
        </th>

        <?php foreach ($cc as $row_key => $row){ ?>
    
            <tr>
            <td style="background:#CCC;"><b><?php echo $row_key+1;?></b></td>

            <?php foreach ($row as $col_key => $col){ $key = $cc_key*10000 + $col_key*1000 + $row_key; ?>

                <?php if ($col == -1) {echo "<td></td>"; continue;}?>

                <td style="background:<?php echo $colours[$col_key];?>" width=230>
                    <canvas id="mycanvas_<?php echo $key;?>" width="400" height="40"></canvas>
                    <text style="display:inline;" id="value_<?php echo $key;?>">0</text>
                </td>                        

                <script type="text/javascript">

                    // Random data
                    var line_<?php echo $key;?> = new TimeSeries();
                    var line_<?php echo $key;?>_threshold = new TimeSeries();
                    var line_<?php echo $key;?>_spikes = new TimeSeries();

                    setInterval(function() {
                        // Append Values (thresholds)
                        line_<?php echo $key;?>.append(new Date().getTime(), document.getElementById('value_'+<?php echo $key;?>).innerHTML);
                        /*
                        // Append Values (spikes)
                        try {
                            line_<?php echo $key;?>_spikes.append(new Date().getTime(), 0);
                            if (parseInt(document.getElementById('value_'+<?php echo $key;?>).innerHTML) <= 1 && parseInt(line_<?php echo $key;?>.data[line_<?php echo $key;?>_spikes.data.length-2][1]) > 50){
                                line_<?php echo $key;?>_spikes.data[line_<?php echo $key;?>_spikes.data.length-2][1] = 100;
                            }
                        }
                        catch {
                            line_<?php echo $key;?>_spikes.append(new Date().getTime(), 0);
                        }
                        */
                        // Slice
                        line_<?php echo $key;?>.data = line_<?php echo $key;?>.data.slice(-100,line_<?php echo $key;?>.data.length);
                        //line_<?php echo $key;?>_spikes.data = line_<?php echo $key;?>_spikes.data.slice(-50,line_<?php echo $key;?>.data.length);
                    }, 100);

                    var chart = new SmoothieChart({interpolation:'step', grid:{fillStyle:'#ffffff', strokeStyle:'#999', sharpLines:true, millisPerLine:200, verticalSections:6},labels:{fillStyle:'#000000'},maxValue:100,minValue:0});
                    chart.addTimeSeries(line_<?php echo $key;?>, {lineWidth:2,strokeStyle:'rgba(255,0,8,0.92)',fillStyle:'rgba(255,0,0,0.27)'});
                    chart.addTimeSeries(line_<?php echo $key;?>_spikes, {lineWidth:2,strokeStyle:'rgba(0,0,255,0.92)',fillStyle:'rgba(0,0,255,0.27)'});
                    chart.streamTo(document.getElementById('mycanvas_<?php echo $key;?>'), 100);

                </script>

            <?php } ?>

            </tr>

        <?php 
        } 

        // Display Images at each layer

        foreach ($image_grid as $cc_key => $cc){
        echo "<tr style='height:180px;'><td style='background:#CCC;'>Image</td>";
            foreach ($cc as $row_key => $row){

                echo '<td style="background:'.$colours[$row_key].'"><center><table><tr>';

                $grid_size = 5;

                foreach ($row as $col_key => $col){
                    $key = $cc_key*10000 + $row_key*1000 + $col_key; 
                    echo '<td style="width:30px; height:100px;" id="value_'.$key.'td"><center>';
                    echo '<text style="display:inline;" id="value_'.$key.'txt">0</text>';
                    echo "</center></td>";
                    if (($col_key+1) % 3 == 0) { if ($col_key == sizeof($row)-1) { echo "</tr>";} else {echo "</tr><tr>";}}
                }

                echo "</center></table></td>";
            }
        }
        echo "</tr></table>";

        ?>

        </table>

    <?php } ?>



    <script type="text/javascript">

        function updateCurrentValues(url) {
          var xhttp = new XMLHttpRequest();
          var pixel_value
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var obj = JSON.parse(this.responseText);
                document.getElementById('counter').innerHTML = parseInt(document.getElementById('counter').innerHTML) + 1;
                for (var k in obj){
                    if (obj.hasOwnProperty(k)) {
                        try {
                            document.getElementById('value_'+k).innerHTML = obj[k];
                            pixel_value = document.getElementById('value_'+k+'txt').innerHTML;
                            pixel_value = (2*pixel_value/3 + obj[k]/3);
                            document.getElementById('value_'+k+'txt').innerHTML = parseInt(pixel_value);
                            document.getElementById('value_'+k+'td').style = "width:30px; background: rgb("+parseInt(pixel_value*2.55)+","+parseInt(pixel_value*2.55)+","+parseInt(pixel_value*2.55)+");";
                        }
                        catch{
                            break;
                        }
                    }
                }
            }
          };
          xhttp.open("GET", "poll_ps.php", true);
          xhttp.send();
        }

        setInterval(function() {
            updateCurrentValues();
        }, 25);

        function filter_spikes(data) {
            return data >= 100;
        }

    </script>

  </body>
</html>

<?php

function getBetween($content,$start,$end){
   $r = explode($start, $content);
   if (isset($r[1])){
       $r = explode($end, $r[1]);
       return $r[0];
   }
   return '';
}

?>
