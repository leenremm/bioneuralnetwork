=====================================================================

Author: Leen Remmelzwaal
Date: 25th May 218
License: CC BY-SA
Affiliation: University of Cape Town, South Africa

=====================================================================

**Install Latex**

- sudo apt-get install lexlive

**Install Gummi (Latex Editor)**

- sudo add-apt-repository ppa:gummi/gummi
- sudo apt-get update
- sudo apt-get install gummi

**Edit Latex file**

- gummi

**Compile PDF version**

- pdflatex -output-directory=pdf paper.tex
