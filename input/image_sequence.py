import cv2
import socket
import numpy as np
import time
from threading import Thread

# ======================================================================

webcam_port = 7777 # Random Image
height = 3
width = 3
loop_counter = 0
delay = 0.2 # seconds

# ======================================================================

def send_signal(loop_counter, image_array):

    rows,cols = image_array.shape
    counter = 0
    for i in range(rows):
        for j in range(cols):
            port = 10000 + counter
            send_value = 20*int(image_array[i][j])
            message = "%s/%s"%(webcam_port,send_value)
            counter += 1
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect(("localhost", port))
                s.sendall(str(message))
                s.close()
                #print "[%s:%s]"%(port,send_value),
            except:
                #print "[Failed to send: %s: %s]"%(port,send_value),
                pass
    print "Signal Sent (Counter: "+str(int(loop_counter))+")"

def generate_image(shape, ones):
    size = np.product(shape)
    board = np.zeros(size, dtype=np.int)
    i = np.random.choice(np.arange(size), ones)
    board[i] = 1
    return board.reshape(shape)

# ======================================================================

threads = []

loop_counter = 1
image_obj = generate_image((height,width), 1)

while True:

    # Generate image
    if loop_counter % 3 == 0:
        image_obj = generate_image((height,width), 1)

    # Send signal as separate threads
    threads.append(Thread(target=send_signal, args=(loop_counter,image_obj)))
    threads[-1].daemon=False
    threads[-1].start()

    time.sleep(delay)

    loop_counter += 1


exit()

# ======================================================================

