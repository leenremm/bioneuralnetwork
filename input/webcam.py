import cv2
import socket
import numpy as np
import time
from threading import Thread

# ======================================================================

webcam_port = 8888 # Webcam port

height = 5
width = 5

camera = cv2.VideoCapture(0)
'''
camera.set(10, 0)
camera.set(11, 0)
camera.set(12, 0)
camera.set(13, 0)
camera.set(14, 0)
camera.set(15, 0)
'''

loop_counter = 0
loop_step = 5

# ======================================================================

def send_signal(loop_counter, image_array):

    rows,cols = image_array.shape
    counter = 0
    for i in range(rows):
        for j in range(cols):
            port = 10000 + counter
            send_value = 100*int(round(image_array[i][j]/255.0))
            message = "%s/%s"%(webcam_port,send_value)
            counter += 1
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect(("localhost", port))
                s.sendall(str(message))
                s.close()
                #print "[%s:%s]"%(port,send_value),
            except:
                #print "[Failed to send: %s: %s]"%(port,send_value),
                pass
    print "Signal Sent (Counter: "+str(int(loop_counter/loop_step))+")"

# ======================================================================

threads = []

while True:
    return_value,image = camera.read()
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    gray_small = cv2.resize(gray, (width, height)) 
    cv2.imshow('image',gray_small)

    if (loop_counter % loop_step == 0):
        threads.append(Thread(target=send_signal, args=(loop_counter,gray_small)))
        threads[-1].daemon=False
        threads[-1].start()

    if cv2.waitKey(1)& 0xFF == ord('q'):
        break

    loop_counter += 1

camera.release()
cv2.destroyAllWindows()

exit()

# ======================================================================

